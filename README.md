# AtlassianAppUpgradereport

Generates a Confluence table with information about an app upgrade. The table includes release notes, bugs fixed, new features implemented and bugs introduced between the current and new version (but not seen previously).

## Usage


````
atlassian_product_upgrade_table --confluence=5.9.9
<table>
  <tbody>
    <tr>
      <th> </th>
      <th style="text-align: center;">Old Version</th>
      <th class="highlight-grey" colspan="2" data-highlight-colour="grey" style="text-align: left;">New Version</th>
    </tr>
    <tr>
      <th>Confluence</th>
      <td>5.9.9</td>
      <td>
        <span>5.9.10</span>
      </td>
      <td>
      <table>
      <tbody><tr><td>
          <a href="http://confluence.atlassian.com/display/DOC/Confluence+5.9.10+Release+Notes">release notes</a>
          </td>
          <td>
        <ac:structured-macro ac:name="expand">
          <ac:parameter ac:name="title">View Bugfixes..</ac:parameter>
          <ac:rich-text-body>
            <p>
              <ac:structured-macro ac:name="jira">
                <ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
                <ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
                <ac:parameter ac:name="serverId">144880e9-a353-312f-9412-ed028e8166fa</ac:parameter>
                <ac:parameter ac:name="jqlQuery">issuetype=Bug AND project=CONF AND fixVersion in (5.9.10) AND status in (Resolved, Closed, Soaking) ORDER BY votes DESC, priority DESC, key DESC</ac:parameter>
                <ac:parameter ac:name="maximumIssues">40</ac:parameter>
              </ac:structured-macro>
            </p>
          </ac:rich-text-body>
        </ac:structured-macro>
        </td>
        <td>
        <ac:structured-macro ac:name="expand">
          <ac:parameter ac:name="title">View Improvements..</ac:parameter>
          <ac:rich-text-body>
            <p>
              <ac:structured-macro ac:name="jira">
                <ac:parameter ac:name="columns">type,key,summary,votes</ac:parameter>
                <ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
                <ac:parameter ac:name="serverId">144880e9-a353-312f-9412-ed028e8166fa</ac:parameter>
                <ac:parameter ac:name="jqlQuery">issuetype!=Bug AND project=CONF AND fixVersion in (5.9.10) AND status in (Resolved, Closed, Soaking) ORDER BY votes DESC, priority DESC, key DESC</ac:parameter>
                <ac:parameter ac:name="maximumIssues">40</ac:parameter>
              </ac:structured-macro>
            </p>
          </ac:rich-text-body>
        </ac:structured-macro>
        </td>
        <td>
        <ac:structured-macro ac:name="expand">
          <ac:parameter ac:name="title">Recently Reported Bugs..</ac:parameter>
          <ac:rich-text-body>
            <p>These are bugs reported against the last 3 releases (5.9.10, 5.9.9, 5.9.8), that may affect you after the upgrade.</p>
            <p>
              <ac:structured-macro ac:name="jira">
                <ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
                <ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
                <ac:parameter ac:name="serverId">144880e9-a353-312f-9412-ed028e8166fa</ac:parameter>
                <ac:parameter ac:name="jqlQuery">project=CONF AND issuetype=Bug AND affectedVersion in (5.9.10, 5.9.9) AND affectedVersion not in (5.9.8, 5.9.7, 5.9.6, 5.9.5, 5.9.4, 5.9.3, 5.9.2, 5.9.1, 5.8.18, 5.8.17, 5.8.16, 5.8.15, 5.8.14, 5.8.13, 5.8.10, 5.8.9, 5.8.8, 5.8.6, 5.8.5, 5.8.4, 5.8.2, 5.7.6, 5.7.5, 5.7.4, 5.7.3, 5.7.1, 5.7, 5.6.6, 5.6.5, 5.6.4, 5.6.3, 5.6.1, 5.6, 5.5.7, 5.5.6, 5.5.4, 5.5.3, 5.5.2, 5.5.1, 5.5, 5.4.4, 5.4.3, 5.4.2, 5.4.1, 5.4, 5.3.4, 5.3.1, 5.3, 5.2.5, 5.2.4) AND resolution is empty ORDER BY votes DESC, priority DESC, key DESC</ac:parameter>
                <ac:parameter ac:name="maximumIssues">40</ac:parameter>
              </ac:structured-macro>
            </p>
          </ac:rich-text-body>
        </ac:structured-macro>
        </td>
        </tr>
        </tbody>
        </table>
      </td>
    </tr>

</tbody>
</table>

````

## Installation

- Install [atlassian_app_versions](https://bitbucket.org/redradish/atlassian_app_versions) gem as described in its README
- Run:
   ```
git clone https://redradish@bitbucket.org/redradish/atlassian_app_upgradereport.git
cd atlassian_app_upgradereport
bundle exec rake install
   ```
- Run `atlassian_product_upgrade_table` or `atlassian_product_upgradereport_properties`.

Note, there is an in-development `atlassian_product_upgradereport` script, but it requires [Atlassian CLI](https://marketplace.atlassian.com/plugins/org.swift.atlassian.cli/server/overview) and is probably not usable yet.

On Ubuntu you will first need to 'apt-get install zlib1g-dev'. Possible libxml2-dev, libxslt-dev and others for nokogiri.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

