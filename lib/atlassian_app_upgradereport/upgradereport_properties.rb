require 'date'
module AtlassianAppUpgradereport

	def self.htmlstatus(text, colour)
		%Q{<ac:structured-macro ac:name="status" ac:schema-version="1"><ac:parameter ac:name="title">#{text}</ac:parameter><ac:parameter ac:name="colour">#{colour}</ac:parameter></ac:structured-macro>}
	end

	# Return a HTML table with plugin 'before' and 'after' states.
	# Note: we must have plugin info for the deployed version, but plugin info for the new (post-upgrade) version is optional. The idea is that the report would be generated prior to the upgrade, then regenerated immediately after with fuller info.
	def self.pluginstates(app)
		raise "No plugin info for #{app.key}  #{app.currentver}" unless app.plugininfo
		#raise "No plugin info for #{app.key} #{app.newver}" unless app.newplugininfo

		curpluginkeys = Set.new(app.plugininfo.collect(&:key))
		newpluginkeys = Set.new(app.newplugininfo&.collect(&:key))

		curplugins_by_key = app.plugininfo.group_by { |p| p.key }
		newplugins_by_key = app.newplugininfo&.group_by { |p| p.key }

		# { "key" => {:cur => plugindata, :new => plugindata}, ... }
		allplugins_by_key = ( curpluginkeys | newpluginkeys).inject({}) { |all,key|
			# The &.first is safe because each plugin is only represented once in the source plugininfo array
			all[key] = {:cur => curplugins_by_key[key]&.first, :new => (newplugins_by_key ? newplugins_by_key[key]&.first : nil) }
			all
		}

		def self.pluginflags(p)
			return htmlstatus('uninstalled', 'Grey') if !p
			#if p&.updateAvailable && !p&.newVersion&.version then
			#	binding.pry
			#end
			(!p&.enabled ? htmlstatus('disabled', 'Grey') : "") +
                            (p&.incompatible ? (p&.statusDataCenterCompatible ? htmlstatus('incompatible', 'Red') : htmlstatus('dc-incompatible', 'Blue')) : "") +
				# It seems that updateAvailable cannot be trusted - we also need to check for newVersion
				(p&.updateAvailable && p&.newVersion ? htmlstatus('updatable to ' + p&.newVersion&.version, 'Yellow') : "") +
				(p&.licenseDetails&.evaluation ? htmlstatus('eval', 'Yellow') : "") +
				(p&.licenseDetails&.error == "EXPIRED" ? htmlstatus('expired on ' + p.licenseDetails.expiryDateString, 'Red') : p&.licenseDetails&.error ? htmlstatus(p.licenseDetails.error, "Red") : "") 
		end


		(
			%Q{
<p>
<h2>Plugin State Summary</h2>
			<table>
			  <colgroup>
			    <col/>
			    <col/>
			    <col/>
			    <col/>
			    <col/>
			  </colgroup>
			  <tbody>
			    <tr>
			      <th colspan="1">Plugin</th>
			      <th colspan="1">Old Version</th>
			      <th colspan="1">Old Status</th>
			      <th colspan="1">New Version</th>
			      <th colspan="1">New Status</th>
			    </tr>
			} + allplugins_by_key.collect do |key,h|
				cur = h[:cur]
				new = h[:new]
				%Q{<tr>} +
					%Q{<th><p><a href="https://marketplace.atlassian.com/plugins/#{new&.key || cur.key}?hosting=server">#{new&.name || cur.name}</a></p></th>} +
					%Q{<td><p>#{cur&.version}</p></td>} +
					%Q{<td><p>#{pluginflags(cur)}</p></td>} +
					%Q{<td><p>#{new&.version}</p></td>} + 
					%Q{<td><p>#{pluginflags(new)}</p></td>} +
					%Q{</tr>} 
			end.join +
			%Q{</tbody>
</table>
</p>}
		).gsub(/\n/, '')  # Strip newlines, since this is going to end up as an enormous arg string that mustn't span lines
	end

	def self.relnotes_xhtml(versions)
		versions.inject({}) { |all, app| 
			(all[app.relNotes] ||= []) << app; all
		}.collect { |link, apps| 
			"<a href=\"#{link}\">" + apps.collect { |a| a.version }.join(', ') + '</a>'
		}.join(", ")
	end

        # Some plugin description, like that for 'Advanced Roadmaps for Jira', contain invalid XML like '<br>', which breaks confluence. This is where such messes should be fixed.
        def self.xmlsanitise(text)
          text.gsub!("<br>", "<br/>")
          binding.pry if text =~ /<.+>/ 
        end

	# Generates the plugin test details section at the end of the report. The table has two columns. The left column includes a child page showing the 'expected' output for the plugin. The right column is blank, and must be filled in by the tester later.
	def self.upgradereport_plugin_details(prod)

		# prod.plugininfo is an array of OpenStructs.  At one point we needed prod.plugininfo.to_h?.empty. Not sure why, as now that breaks normal working upgrades
		if prod.plugininfo.empty? then return 'No plugins'; end
		if prod.plugininfo.empty? then return 'No plugins'; end
		%Q{<table>} +
			%Q{<colgroup>} +
			%Q{<col/>} +
			%Q{<col/>} +
			%Q{</colgroup>} +
			%Q{<tbody>} + 
			prod.plugininfo.collect { |p|
				%Q{<tr><td colspan="2">} + 
					%Q{<div class="content-wrapper"><p><h3><ac:structured-macro ac:name="anchor" ac:schema-version="1"><ac:parameter ac:name="">#{p.key}</ac:parameter></ac:structured-macro>#{p.name}</h3></p><p><ul><li><code>#{p.key}</code></li><li>#{xmlsanitise(p.description)}</li><li><a href="https://marketplace.atlassian.com/plugins/#{p.key}">marketplace link</a></li></ul></p></div>} +
					%Q{</td></tr>} +
					%Q{<tr>} +
					%Q{<td>#{p.version}</td>} +
					%Q{<td>#{p.updateAvailable && p&.newVersion ? p.newVersion.version : ""}</td>} +
					%Q{</tr>} +
					%Q{<tr>} +
					%Q{<td>} + 
					(
						p.enabled ? 
						(
							%Q{<ac:structured-macro ac:macro-id="95012817-4348-4548-9d9b-5490ad0b2fe6" ac:name="include" ac:schema-version="1">}+
							%Q{<ac:parameter ac:name="">} +
							%Q{<ac:link>} +
							%Q{<ri:page ri:content-title="_plugin_#{p.key}"/>} +
							%Q{</ac:link>} +
							%Q{</ac:parameter>} +
							%Q{</ac:structured-macro>}
						)
						: %Q{<p style="text-align: center;"><em>Disabled</em></p>}
					) +
					%Q{</td>} +
					%Q{<td><ac:task-list>} +
					%Q{<ac:task>} +
					%Q{<ac:task-id>105</ac:task-id>} +
					%Q{<ac:task-status>incomplete</ac:task-status>} +
					%Q{<ac:task-body>Verified working</ac:task-body>} +
					%Q{</ac:task>} +
					%Q{</ac:task-list>} +
					%Q{</td>} +
					%Q{</tr>}
			}&.join() +
			%Q{</tbody>} +
			%Q{</table>}
	end

	# Generates the Summary table with rows: 'Old Version', 'New Version', 'Release Notes', 'Bugfixes', etc
	def self.upgradereport_properties(applink, prod, otherProds, fromVer, toVer=nil, upgradeDate=nil)
		# prod.versions(fromVer, toVer)
		prod.plugininfo
		if !toVer then toVer = prod.versions.first.to_s; end
		tbl = %Q{<ac:structured-macro ac:macro-id="36578ea1-1ea5-4d7d-a6f3-52a844aee1f9" ac:name="details" ac:schema-version="1">
  <ac:rich-text-body>
    <table>
      <colgroup>
	<col style="width: 15;"/>
	<col style="width: 85;"/>
      </colgroup>
      <tbody>
	<tr>
	  <th>Summary</th>
	  <td>
            <p></p>
	  </td>
	</tr>
	<tr>
	  <th colspan="1">Old Version</th>
	  <td colspan="1">#{fromVer}</td>
		</tr>
	<tr>
	  <th colspan="1">New Version</th>
	  <td colspan="1">#{toVer}</td>
	</tr>
	<tr>
	  <th colspan="1">Release Notes</th>
	  <td colspan="1">
		} + relnotes_xhtml(prod.versions(fromVer, toVer)) + 
		%Q{
	  </td>
	</tr>
	<tr>
	  <th colspan="1">Bugfixes</th>
	  <td colspan="1">
	  <p>
	    <ac:structured-macro ac:macro-id="d0835a86-ab06-49ff-929f-feceb38966e8" ac:name="expand" ac:schema-version="1">
	      <ac:parameter ac:name="title">View Security Bugfixes..</ac:parameter>
	      <ac:rich-text-body>} +
		%Q{<p>} +
	      %Q{<ac:structured-macro ac:name="jira">
		<ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
		<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
		<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
		<ac:parameter ac:name="jqlQuery">#{prod.securityBugsJQL(fromVer, toVer)}</ac:parameter>
		<ac:parameter ac:name="maximumIssues">40</ac:parameter>
	      </ac:structured-macro>} + 
		%Q{</p>} +
	       %Q{</ac:rich-text-body>
	    </ac:structured-macro>
	    </p>
	  <p>
	    <ac:structured-macro ac:macro-id="d0835a86-ab06-49ff-929f-feceb38966e8" ac:name="expand" ac:schema-version="1">
	      <ac:parameter ac:name="title">View All Bugfixes..</ac:parameter>
	      <ac:rich-text-body>} + 
		%Q{<p>} +
	      %Q{<ac:structured-macro ac:name="jira">
		<ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
		<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
		<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
		<ac:parameter ac:name="jqlQuery">#{prod.bugsJQL(fromVer, toVer)}</ac:parameter>
		<ac:parameter ac:name="maximumIssues">40</ac:parameter>
	      </ac:structured-macro>} +
		%Q{</p>} +
	      %Q{</ac:rich-text-body>
	    </ac:structured-macro>
	    </p>
	  </td>
	</tr>
	<tr>
	  <th colspan="1">Improvements</th>
	  <td colspan="1">
	    <ac:structured-macro ac:macro-id="8ad777c6-9d8f-44b9-88fd-70ac1b2cb11c" ac:name="expand" ac:schema-version="1">
	      <ac:parameter ac:name="title">View Improvements..</ac:parameter>
	      <ac:rich-text-body>} +
		%Q{<p>} +
	      %Q{<ac:structured-macro ac:name="jira">
		<ac:parameter ac:name="columns">type,key,summary,votes</ac:parameter>
		<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
		<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
		<ac:parameter ac:name="jqlQuery">#{prod.featuresJQL(fromVer, toVer)}</ac:parameter>
		<ac:parameter ac:name="maximumIssues">40</ac:parameter>
	      </ac:structured-macro>} +
		%Q{</p>} +
	      %Q{</ac:rich-text-body>
	    </ac:structured-macro>
	  </td>
	</tr>
	<tr>
	  <th colspan="1">Recently Reported Bugs</th>
	  <td colspan="1">
	    <ac:structured-macro ac:macro-id="d0835a86-ab06-49ff-929f-feceb38966e8" ac:name="expand" ac:schema-version="1">
	      <ac:parameter ac:name="title">View new bug reports..</ac:parameter>
	      <ac:rich-text-body>} +
		%Q{<p>These are bugs reported against the last 3 releases (#{prod.versions(fromVer, toVer).first(3).join(", ")}), that may affect you after the upgrade.</p>} +
		%Q{<p>} +
	      %Q{<ac:structured-macro ac:name="jira">
		<ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
		<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
		<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
		<ac:parameter ac:name="jqlQuery">#{prod.recentbugsJQL(fromVer, toVer)}</ac:parameter>
		<ac:parameter ac:name="maximumIssues">40</ac:parameter>
	      </ac:structured-macro>} +
		%Q{</p>} +
	      %Q{</ac:rich-text-body>
	    </ac:structured-macro>
	  </td>
	</tr>
	<tr>
		  <th colspan="1">Plugins</th>
		  <td colspan="1">#{pluginstates(prod)}</td>
	 </tr>
	<tr>
		  <th colspan="1">Post-Upgrade Verification Checklist</th>
		  <td colspan="1">
		    <ac:task-list>
	<ac:task>
	  <ac:task-id>1</ac:task-id>
	  <ac:task-status>incomplete</ac:task-status>
	  <ac:task-body>} +
		  %Q{<ac:link ac:anchor="Pre-upgradeBackups">
		    <ac:plain-text-link-body><![CDATA[Pre-upgrade backups]]></ac:plain-text-link-body>
		  </ac:link>} + 
		  %Q{</ac:task-body>
	</ac:task>} + (
		otherProds.collect { |other|
			puts "#{prod.name} -> #{other.name}"
			%Q{<ac:task>
	<ac:task-id>12</ac:task-id>
	<ac:task-status>incomplete</ac:task-status>
	<ac:task-body>}+
	%Q{<ac:link ac:anchor="#{other.name}2#{prod.name}">
			    <ac:plain-text-link-body><![CDATA[#{other.name} contents showing in #{prod.name}]]></ac:plain-text-link-body>
			  </ac:link>}+
			  %Q{</ac:task-body>
	</ac:task>
			  }
		}.join("\n")
	  ) +
	  (

		  prod.key=~/jira/ ?
		  %Q{<ac:task>
	<ac:task-id>3</ac:task-id>
	<ac:task-status>incomplete</ac:task-status>
	<ac:task-body>Email verified working</ac:task-body>
	</ac:task>
		  } : ""
	  ) +
	 (
		 (prod.key =~ /jira/ or prod.key=='confluence') ?
		 %Q{<ac:task>
	<ac:task-id>16</ac:task-id>
	<ac:task-status>incomplete</ac:task-status>
	<ac:task-body>Staging instance online<span> </span></ac:task-body>
	</ac:task>
		 } : ""
	  ) +
	 %Q{
	<ac:task>
		<ac:task-id>2</ac:task-id>
		<ac:task-status>incomplete</ac:task-status>
		<ac:task-body>Applinks working</ac:task-body>
	</ac:task>
	<ac:task>
		<ac:task-id>2</ac:task-id>
		<ac:task-status>incomplete</ac:task-status>
		<ac:task-body>Backups working</ac:task-body>
	</ac:task>
	<ac:task>
		<ac:task-id>2</ac:task-id>
		<ac:task-status>incomplete</ac:task-status>
		<ac:task-body>IP restrictions removed</ac:task-body>
	</ac:task>
	</ac:task-list>
	</td>
		</tr>
		<tr>
		  <th colspan="1">To Do Next Upgrade</th>
		  <td colspan="1">
		  </td>
		</tr>
		<tr>
		  <th colspan="1">Upgrade Date</th>
		  <td colspan="1">} + 
			  (upgradeDate ? %Q{<time datetime="#{Date.parse(upgradeDate).strftime("%Y-%m-%d")}"/>} : '') +
		  %Q{</td>
		</tr>
	      </tbody>
	    </table>
	  </ac:rich-text-body>
	</ac:structured-macro>}

	 return tbl
	end

	## Generates a Confluence table (XHTML), identifying bugs fixed and features implemented in a particular version range, typically that of the latest product upgrade.
	#
	#def self.upgradereport_properties(applink, prod, otherProds, fromVer, toVer=nil)
	def self.product_upgrade_table(applink, products)
		prefix = %Q{
	<table>
	  <tbody>
	    <tr>
	      <th> </th>
	      <th style="text-align: center;">Old Version</th>
	      <th class="highlight-grey" colspan="2" data-highlight-colour="grey" style="text-align: left;">New Version</th>
	    </tr>}
		tbl=""
		body = products.collect { |prod|
			fromVer = prod.currentver.version
			toVer = prod.newver.version
			%Q{
	    <tr>
	      <th>#{prod.name}</th>
	      <td>#{prod.currentver.version}</td>
	      <td>
		<span>#{prod.newver.version}</span>
	      </td>
	      <td>
	      <table>
	      <tbody><tr><td>
			} + prod.versions(fromVer, toVer).collect { |v| 
				"<p><a href='#{v.relNotes}'>#{v.version} RelNotes</a></p>"
			}.join("") +
			%Q{
		  </td>
		  <td>
		  <p>
		<ac:structured-macro ac:name="expand">
		  <ac:parameter ac:name="title">View Security Fixes..</ac:parameter>
		  <ac:rich-text-body>
		    <p>
		      <ac:structured-macro ac:name="jira">
			<ac:parameter ac:name="columns">type,key,summary,versions,priority,labels</ac:parameter>
			<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
			<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
			<ac:parameter ac:name="jqlQuery">#{prod.securityBugsJQL(fromVer, toVer)}</ac:parameter>
			<ac:parameter ac:name="maximumIssues">40</ac:parameter>
				</ac:structured-macro>
		    </p>
		  </ac:rich-text-body>
		</ac:structured-macro>
		</p>
		  <p>
		<ac:structured-macro ac:name="expand">
		  <ac:parameter ac:name="title">View Bugfixes..</ac:parameter>
		  <ac:rich-text-body>
		    <p>
		      <ac:structured-macro ac:name="jira">
			<ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
			<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
			<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
			<ac:parameter ac:name="jqlQuery">#{prod.bugsJQL(fromVer, toVer)}</ac:parameter>
			<ac:parameter ac:name="maximumIssues">40</ac:parameter>
				</ac:structured-macro>
		    </p>
		  </ac:rich-text-body>
		</ac:structured-macro>
		</p>
		</td>
		<td>
		<ac:structured-macro ac:name="expand">
		  <ac:parameter ac:name="title">View Improvements..</ac:parameter>
		  <ac:rich-text-body>
		    <p>
		      <ac:structured-macro ac:name="jira">
			<ac:parameter ac:name="columns">type,key,summary,votes</ac:parameter>
			<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
			<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
			<ac:parameter ac:name="jqlQuery">#{prod.featuresJQL(fromVer, toVer)}</ac:parameter>
			<ac:parameter ac:name="maximumIssues">40</ac:parameter>
				</ac:structured-macro>
		    </p>
		  </ac:rich-text-body>
		</ac:structured-macro>
		</td>
		<td>
		<ac:structured-macro ac:name="expand">
		  <ac:parameter ac:name="title">Recently Reported Bugs..</ac:parameter>
		  <ac:rich-text-body>
		    <p>These are bugs reported against the last 3 releases (#{prod.versions[0..2].join(", ")}), that may affect you after the upgrade.</p>
		    <p>
		      <ac:structured-macro ac:name="jira">
			<ac:parameter ac:name="columns">type,key,summary,versions,votes,priority</ac:parameter>
			<ac:parameter ac:name="server">Atlassian JIRA</ac:parameter>
			<ac:parameter ac:name="serverId">#{applink}</ac:parameter>
			<ac:parameter ac:name="jqlQuery">#{prod.recentbugsJQL(fromVer, toVer)}</ac:parameter>
			<ac:parameter ac:name="maximumIssues">40</ac:parameter>
				</ac:structured-macro>
		    </p>
		  </ac:rich-text-body>
		</ac:structured-macro>
		</td>
		</tr>
		</tbody>
		</table>
	      </td>
	    </tr>
			}
		}.join("")
		suffix = "
	</tbody>
	</table>"
		return prefix + body + suffix
	end
end
