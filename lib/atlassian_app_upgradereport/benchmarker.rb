

class Benchmarker
	def initialize
		@results = []
	end

	def count
		@results.size
	end

	def measure(info, &block)
		t0 = Time.now
		output = yield
		t1 = Time.now
		dt = t1 - t0
		@results << [info, dt]
		output
	end
	def report
		puts "==============================="
		@results.each do |msg, time|
			printf "%s: %f\n", msg, time
		end
		puts "==============================="
	end
end
