require 'atlassian_app_versions'

# Represents an app deployment at a particular version, and a new version we want to upgrade to. Uses the profile directory to look up version info.
class AppDeployment < AtlassianAppVersions::App
	attr_accessor :longname, :currentver, :newver

	# appstr is a string of the format: longname:product:version, e.g. 'jira:jira-software:8.0.1'
	# profiledir is a path to the profile directory (containing plugindata/)
	def initialize(appstr, profiledir, productionhost)
		if appstr.count(':') != 3 then
			$stderr.puts "Invalid app: #{appstr}. Format is longname:appkey:currentver:newver"
			exit 1
		end
		longname, appkey, currentver, newver = appstr.split(':')
		raise "Invalid long name for app, in '#{appstr}'" unless longname =~ /[a-zA-Z]{3,}+/
		raise "Invalid appkey, in '#{appstr}'" unless ['jira-software', 'jira-core', 'confluence', 'crowd', 'fisheye'].member? appkey
		raise "Invalid old version for app, in '#{appstr}'" unless currentver =~ /[0-9\.]+/
		raise "Invalid new version for app, in '#{appstr}'" unless newver =~ /[0-9\.]+/
		@longname = longname
		super(appkey)
		@currentver = AtlassianAppVersions::AppVersion.new(currentver, @app)
		@newver = AtlassianAppVersions::AppVersion.new(newver, @app)
		@profiledir = Pathname.new(profiledir) 
		@productionhost = productionhost
	end

	# Get an OpenStruct of ATL_* profile variables (the same ones the atl_* scripts use)
	def profileinfo
		if !@profileinfo then
			require 'shellwords'
			rawcmd = %Q{export ATL_MANAGE=/opt/atl_manage
			export ATL_PROFILEDIR=#{@profiledir}
			export HOSTNAME=#{@productionhost}
			source $ATL_MANAGE/setup.bash &>/dev/null
			source $ATL_MANAGE/bin/atl_app #{@longname} &>/dev/null
			ruby -r json -e 'puts ENV.to_hash.select { |s| s=~/^ATL_/ }.to_json'}
			cmd = Shellwords.escape(rawcmd)
			$stderr.puts "Now running: #{cmd}"
			myjson = %x[bash -c #{cmd}]
			@profileinfo = JSON.parse(myjson, object_class: OpenStruct)
		end
		@profileinfo
	end

	# Profile info in [["k1", "v1"], ["k2", "v2"],...] format for consumption by CliUtils.format_stringpairs_as_cli_replacements
	#def profileinfo_stringpairs
	#	profileinfo.to_h.collect { |k,v| ["@#{k}@", v] }
	#end

	# Return an OpenStruct of plugin info, or nil if the information isn't available in the profile directory.
	def plugininfo
		if !@plugininfo then
			jsonfile = Pathname.new(@profiledir) + "plugindata/#{@longname}/#{@currentver}.json"
			if File.file?(jsonfile) then
				@plugininfo = json2ostruct(jsonfile)
				#@plugininfo =  JSON.parse(IO.read(jsonfile))
			else
				$stderr.puts "Warning: missing current version's plugin file #{jsonfile} in #{Dir.pwd}"
			end
		end
		@plugininfo
	end

	# Get an OpenStruct of info about the new version's plugins
	def newplugininfo
		if !@newplugininfo then
			jsonfile = Pathname.new(@profiledir) + "plugindata/#{@longname}/#{@newver}.json"
			if File.file?(jsonfile) then
				@newplugininfo = json2ostruct(jsonfile)
				#@newplugininfo = JSON.parse(IO.read(jsonfile))
			end
		end
		@newplugininfo
	end

	# private:
	def json2ostruct(jsonfile)
		require 'json'
		require 'ostruct'
		if !File.exist? jsonfile then
			return nil
		end
		return JSON.parse(IO.read(jsonfile), object_class: OpenStruct)
	end
end
