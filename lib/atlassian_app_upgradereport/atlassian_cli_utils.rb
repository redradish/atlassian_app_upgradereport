module CliUtils

	@globalopts = nil
	@benchmark = nil

	def self.benchmark
		@benchmark
	end

	def self.globalopts
		@globalopts
	end

	def self.format_hash_as_cli_replacements(replacementhash, linesep: '❤', kvsep: '→')
          raise "This function should never be called. Atlassian CLI no longer supports colon-separated tokens: https://bobswift.atlassian.net/browse/ACLI-792"
		replacementhash.collect { |k,v|
			binding.pry if !k
			binding.pry if !v
			binding.pry if !kvsep
			"#{k}#{kvsep}#{v}"
		}.join(linesep)
	end

	def self.hash_to_args(replacementhash, kvsep: '→')
		replacementhash.collect { |k,v|
			binding.pry if !k
			binding.pry if !v
			binding.pry if !kvsep
                        ["--findReplace", "#{k}#{kvsep}#{v}"]
                }.flatten
	end

	def self.printgo
		puts @globalopts
	end

	# Return path to confluence.sh
	def self.confluence
		_ = (Pathname.new((@globalopts['--cli-path'] or ENV['HOME']+"/apps/atlassian-cli-current"))) + "redradish_confluence.sh"
		raise "Cannot find «#{_}»" unless _.exist?
		_
	end

	class Page

		# usetarget=true means the page is found on the target server, not the local one (as is the default). The --target{Server,User,Password} args will be used for --server/--user/--password too. This is only useful if the target server has the CLI Connector installed
		def initialize(space:nil, title:nil, id:nil, parentid:nil, usetarget:false)
			raise ArgumentError, "Missing either id «#{id}» or space «#{space}» + title «#{title}»."  unless (space and title) or id
			@space=space
			@title=title
			@id=id
			@parentid=parentid
			# Certain atlassian-cli options, like --target* are used on every command. Store them here
			@confluence = CliUtils::confluence
			@usetarget = usetarget
			@benchmark = CliUtils.benchmark
			@globalopts = CliUtils.globalopts
		end

		# confluence.sh args to use on every request. Primarily this is to support the usetarget=true constructor flag
		def default_args
			if @usetarget && @globalopts['--targetServer'] then
			 	["--server", @globalopts['--targetServer'], "--user", @globalopts['--targetUser'], "--password", @globalopts['--targetPassword']]
			else 
				[]
			end
		end

		def confluence(args=[], validresponses=nil, &validate)
			#puts "================================= #{@benchmark.count}"

			@benchmark.measure "#{args}" do
				cmd = [@confluence.to_s]
				cmd += default_args
				cmd += args.map{ |a| "#{a}" }
				cmd += @globalopts.clone.keep_if { |k,v| k=~/--target/ && v }.flatten
				puts "Running #{cmd.collect { |c| "'#{c}'" }.join(' ')}"
				outstr, errstr, status = Open3.capture3({"HOME" => ENV["HOME"]}, *cmd)
				if status.exitstatus != 0 then
					raise "Failed to run #{@confluence}. #{errstr}"
				end
				if validresponses then
					validresponses.each do |regex|
						scan = outstr.scan(regex)
						if !scan.empty? then
							return scan[0]
						end
					end
					raise "Command «#{cmd.join(' ')}» returned \n\t«#{outstr}», which is not an expected response:\n\t#{validresponses.join(" or ")}"
				end
				if block_given? then
					validate.call(outstr)
				else
					outstr
				end
			end
		end

		def get
			args = ["--action", "getPage", "--quiet", "--outputType", "json"]
			if @id and @id > 0 then
				args += ["--id", "#{@id}"]
			else
				args += ["--space", @space, "--title", @title]

			end
			# Set a bunch of instance variables.
			# Previously we set variables:
			#
			#  {"@page_id"=>"184352832",
			#  "@page_title_"=>"Upgrade Report Template",
			# "@space_key"=>"TEMPLATE",
			# "@version"=>"12",
			# "@parent_id"=>"184352823",
			# "@parent_title_"=>"Report Templates Home",
			# "@url"=>"https://wiki.redradishtech.com/display/TEMPLATE/Upgrade+Report+Template",                                                                         
			# "@creator"=>"jturner",
			# "@created"=>"31/01/19 1:12 AM",
			# "@modifier_"=>"jturner",
			# "@modified_"=>"22/02/19 5:41 PM",
			# "@home_page"=>"No",
			# "@status_"=>"current"}
			

			confluence(args) { |outstr|
				JSON.parse(outstr, symbolize_names: true).
					transform_keys { |k| k.downcase.to_s.gsub(" ", "_") }
					.each { |k,v|
						# STDERR.puts "Setting @#{k} to #{v}"
						instance_variable_set("@#{k}",v)
						Page.attr_accessor k.to_sym
					}
			}

			# Shortcuts (and retain backwards_compat)
			@id = @pageid.to_i if @pageid
			# @id = @page_id.to_i if @page_id
			@parentid = @parent_id.to_i if @parent_id
			self
		end

		def labels
			args = ["--action", "getLabelList"]
			if @id then
				args += ["--id", "#{@id}"]
			else
				args += ["--space", @space, "--title", @title]

			end
			if !@labels then
				confluence(args) { |outstr|
					@labels = outstr.split(/\n/).reject { |l| l =~ /labels in list/ }.map { |s| s.gsub(/^"(.*)"$/, '\1') }
				}
			end
			@labels

			#@id = outstr&.split(/\n/)&.grep(/^Page id/)&.first&.split(': ')[1]&.to_i
			#@parentid = outstr&.split(/\n/)&.grep(/^Parent id/)&.first&.split(': ')[1]&.to_i
		end

		def copy(args=[])
			if ! args.member? "--newSpace" then
				# If we aren't copying to a new space then we'll need a new title
				raise ArgumentError, "Missing --newTitle in args #{args}" unless args.member? '--newTitle'
				newtitle = args[args.index('--newTitle')+1]
				raise ArgumentError, "--newTitle («#{newtitle}») must be different to title («#{@title}»). Args: #{args}" unless newtitle != @title
			else
				raise ArgumentError, "Missing --parent in args #{args}" unless args.member? '--parent'
			end
			get unless @parentid
			args.unshift("--action", "copyPage")
			args += ["--title", @title]
			args += ["--space", @space]
			binding.pry if !args.member? "--title"

			# E.g. replaces 'JIRA 7.1.6' with 'JIRA [\d\.]+:JIRA 7.1.6'
			#cmd << "--findReplaceRegex" <<$ $opts["--newTitle"].
			#	       scan(/[^ ]+ [\d\.]+/).
			#	       map { |s| s.gsub(/[\d\.]+/, '[\d\.]+') + ":" + s }.join(",")
			#
			#	       parseProductsFromTitle() returns {"JIRA" => "7.1.6", "Confluence" => 5.9.10" }
			#	       parseProductsFromTitle().collect { |app,ver| "#{app} [\d\.]+:#{app} #{ver}" }.join(",")
			oldtitle, newtitle, newspace, parenttitle, newid = confluence(args, [/Page '([^']+?)' copied to '([^']+?)' in space ([^ ]+?) as child of '([^']+?)'. Page has id (\d+)./]) 
			Page.new(id:newid, space:newspace, title:newtitle)
		end

		def reparent(newparentid)
			get unless @parentid
			return if @parentid == newparentid 
			args = []
			args += ['--action', 'movePage']
			args += ['--id', "#{id}"]
			args += ["--parent", newparentid]
			outstr = confluence(args) 
			$stderr.puts outstr
			@parentid = newparentid
		end

		# Modify page content by adding contnet before (content or file
		# parameters) or after (content2 parameter) current content and
		# applying findReplace logic). If append is speified, the file content
		# will go after the current content.
		def modify(args=[])
			args.unshift("--action", "modifyPage", "--id", "#{id}")

			title, space, id = confluence(args, [/Page '([^']+?)' in space ([^ ]+?) modified. Page has id (\d+).\n/, /Content has not changed. Content was page '([^']+?)' in space: ([^ ]+?). Page has id: (\d+)/])
			self
		end

		def children
			args = []
			args += ["--action", "getPageList"]
			args += ["--id", "#{id}"]
                        #FIX: I don't think we want all descendents. That breaks the first 'templatereport.children' line..
			args += ["--children", "--outputFormat", "999", "--columns", "3, title"]
			#args += ["--descendents", "--outputFormat", "999", "--columns", "3, title"]
			outstr = confluence(args)
			if outstr !~ /\d+ pages in list\n/ then raise "Unexpected output of getPageList: #{outstr}"; end
                        # chomp newlines that inexplicably appear now (May/2020)
                        csv = CSV.parse(outstr.gsub(/^\d+ pages in list\n/, '').chomp, :headers=>true)
			csv.delete_if { |row| row.empty? or row["Title"] == @title }
			csv.collect { |row|
				binding.pry if !row['Title']
				Page.new(space: @space, parentid:@id, id:row['Id'], title:row['Title'])
			}
		end

		def source
			args = []
			args += ["--action", "getPageSource"]
			args += ["--id", id]
			confluence(args)
		end
		def source=(src)
			args = []
			args += ["--action", "storePage"]
			args += ["--id", id]
			args += ["--content", src]
			args += ["--noConvert"]
			confluence(args)
		end

		def create
			raise unless @parentid
			raise unless @title
			raise unless @space
			args=[]
			args=["--action", "addPage"]
			args += ["--title", @title]
			args += ["--space", @space]
			args += ["--parent", @parentid]
			confluence(args)
		end

		def locked?
			begin
				get
				return labels.include?("locked")
			rescue
				puts "#{@pagetitle} page doesn't exist yet"
			end
			false
		end

		def unlocked?
			begin
				get
				return labels.include?("unlocked")
			rescue
				puts "#{@pagetitle} page doesn't exist yet"
			end
			true
		end

		def to_s
			"id=#{@id} space=#{@space} title=«#{@title}»"
		end

		def url
			"#{BASEURL}/pages/viewpage.action?pageId=#{@id}"
		end

		def parentid
			get unless @parentid
			@parentid
		end

		def id
			get unless @id
			@id.to_i
		end

		def space
			get unless @space
			@space
		end

		def title
			raise "Asked for #{self} Title, but none is set." unless @title
			@title
		end

		def exists?
			begin
				get
				return true
			rescue
				return false
			end
		end

	end

	# I think this is unused
	def self.printpage(page, indent="")
		puts indent + page.title
		yield indent, page
		page.children.each { |child|
			printpage(child, "\t" + indent)
		}
	end

end
