require 'test_helper'
require 'atlassian_app_versions'
require 'pry'
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'atlassian_app_upgradereport'
include CliUtils

class AtlassianAppUpgradereportTest < Minitest::Test
	def test_that_it_has_a_version_number
		refute_nil ::AtlassianAppUpgradereport::VERSION
	end

	PROFILE_DIR = Pathname.new(__FILE__) + '../profiledir'

	def test_appdeployment
		app = AppDeployment.new('jira:jira-software:7.7.2:7.13.0', PROFILE_DIR)
		assert_equal 'jira', app.profileinfo.ATL_USER
		assert_equal 'jira.atlas.llnw.com', app.profileinfo.ATL_LONGNAME
		assert_equal 15, app.plugininfo.size
		refute_nil app.plugininfo.find { |p| p.key=="com.almworks.jira.structure" }
	end

	def test_cliutils
		assert_equal "<ac:task-status>complete</ac:task-status>→<ac:task-status>incomplete</ac:task-status>❤@ATL_OLDVER@→1.0❤@ATL_NEWVER@→2.0❤@ATL_SHORTNAME@→JRA", 
			CliUtils.format_hash_as_cli_replacements({
				"<ac:task-status>complete</ac:task-status>" => "<ac:task-status>incomplete</ac:task-status>",
				"@ATL_OLDVER@" => "1.0",
				"@ATL_NEWVER@" => "2.0",
				"@ATL_SHORTNAME@" => "JRA"
		})

		app = AppDeployment.new('jira:jira-software:7.7.2:7.13.0', PROFILE_DIR)
	end

end
