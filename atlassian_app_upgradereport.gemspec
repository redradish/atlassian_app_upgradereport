# coding: utf-8
lib = File.expand_path('../lib', __FILE__)


$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'atlassian_app_upgradereport/version'

Gem::Specification.new do |spec|
  spec.name          = "atlassian_app_upgradereport"
  spec.version       = AtlassianAppUpgradereport::VERSION
  spec.authors       = ["Jeff Turner"]
  spec.email         = ["jeff@redradishtech.com"]

  spec.summary       = %q{Generate Confluence reports showing bugs and features fixed with Atlassian app upgrades}
  spec.description   = %q{Queries Atlassian's bug tracker, jira.atlassian.com, to show exactly what bugs and features were fixed between two app releases.}
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-byebug"
  spec.add_runtime_dependency "docopt"
  spec.add_dependency "atlassian_app_versions", "~> 0.2.8"
  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "recursive-open-struct"
end
